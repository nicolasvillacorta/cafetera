package com.captton.cafetera;

public class Cafetera {
	
	private int capacidadMaxima;
	private int cantidadActual;
	
	// Constructores
	public Cafetera() {
		this.capacidadMaxima = 1000;
		this.cantidadActual = 0;
	}
	
	
	public Cafetera(int capMax) {
		this.capacidadMaxima = capMax;
		this.cantidadActual = capMax;
		
	}
	
	
	public Cafetera(int capMax, int capAct) {
		if(capAct>capMax) {
			this.capacidadMaxima = capMax;
			this.cantidadActual = capMax;
		}else {
			this.capacidadMaxima = capMax;
			this.cantidadActual = capAct;
		}
			
		
	}

	// Getters y Setters
	public int getCapacidadMaxima() {
		return capacidadMaxima;
	}


	public void setCapacidadMaxima(int capacidadMaxima) {
		this.capacidadMaxima = capacidadMaxima;
	}


	public int getCantidadActual() {
		return cantidadActual;
	}


	public void setCantidadActual(int cantidadActual) {
		this.cantidadActual = cantidadActual;
	}
	
	// Metodos
	public void llenarCafetera() {
		this.cantidadActual = this.capacidadMaxima;
	}
	
	public void servirTaza(int cc) {
		if(this.cantidadActual<=cc) {
			this.cantidadActual = 0;
		}else {
			this.cantidadActual = this.cantidadActual-cc;
		}	
	}
	
	public void vaciarCafetera() {
		this.cantidadActual = 0;
	}
	
	public void agregarCafe(int cc) {
		this.cantidadActual = this.cantidadActual+cc;
	}
	
	

}
