package com.captton.programa;
import com.captton.cafetera.Cafetera;

public class Inicio {

	public static void main(String[] args) {
		Cafetera Cafi = new Cafetera();
		System.out.println("Cap max debe ser 1000 y actual 0");
		System.out.println(Cafi.getCapacidadMaxima());
		System.out.println(Cafi.getCantidadActual());
		System.out.println("---------------");
		Cafetera Cafi2 = new Cafetera(500);
		System.out.println("Cap max debe ser la misma");
		System.out.println(Cafi2.getCapacidadMaxima());
		System.out.println(Cafi2.getCantidadActual());
		System.out.println("---------------");
		System.out.println("Seteo maxima y actual, si actual es mayor, se ajusta a la maxima");
		Cafetera Cafi3 = new Cafetera(200, 210);
		System.out.println(Cafi3.getCapacidadMaxima());
		System.out.println(Cafi3.getCantidadActual());
		Cafi3.servirTaza(70);
		System.out.println("--------\nResta lo que sirvio en la taza (hasta 0)");
		System.out.println(Cafi3.getCantidadActual());
		System.out.println("--------\nVacia la cafetera");
		Cafi2.vaciarCafetera();
		System.out.println(Cafi2.getCantidadActual());
		Cafi2.llenarCafetera();
		System.out.println("--------\nLlena la cafetera");
		System.out.println(Cafi2.getCantidadActual());
	}

}
